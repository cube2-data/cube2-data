(C) Alon Zakai ('Kripken')

Licensed under CC-BY-SA 3.0  -  http://creativecommons.org/licenses/by-sa/3.0/

These are a file-for-file replacement of the original files in Sauerbraten, created
from scratch.

bullet.png, scorch.png: Gregor Koch

plasma.png (taken from Sauerbraten packages/particles): Slawomir 'Q009' Blauciak CC-BY-SA 3.0
